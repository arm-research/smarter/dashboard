package testing

import (
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

func CreateNodeStruct(nodeName string) *v1.Node {
	return &v1.Node{
		ObjectMeta: metaV1.ObjectMeta{Name: nodeName},
		Spec: v1.NodeSpec{
			Unschedulable: true,
		},
	}
}

// MapStringToPointerToRuntimeObject :: []string -> func(string) *v1.Node -> []runtime.Object
// Fmap over list functor - parametrised to needed types, due to lack of generic programming
func MapStringToPointerToRuntimeObject(vs []string, f func(string) *v1.Node) []runtime.Object {
	vsm := make([]runtime.Object, len(vs))
	for i, v := range vs {
		result := f(v)
		vsm[i] = runtime.Object(result)
	}
	return vsm
}

// CreateListOfNodes :: []string -> []runtime.Object
func CreateListOfNodes(names []string) []runtime.Object {
	return MapStringToPointerToRuntimeObject(names, CreateNodeStruct)
}