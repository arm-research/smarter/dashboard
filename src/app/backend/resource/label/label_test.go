package label

import (
	"testing"

	"github.com/kubernetes/dashboard/src/app/backend/resource/dataselect"
	"github.com/kubernetes/dashboard/src/app/backend/resource/node"
	testingUtilities "github.com/kubernetes/dashboard/src/app/backend/testing"
	"k8s.io/client-go/kubernetes/fake"
)

func TestPuttingLabel(t *testing.T) {
	nodesObjects := testingUtilities.CreateListOfNodes([]string{"node"})
	fakeClient := fake.NewSimpleClientset(nodesObjects...)
	SetNodeLabel(fakeClient, "node", []string{"my-label"}, []string{"myContents"})
	result, err := node.GetNodeListByLabel(fakeClient, dataselect.NoDataSelect, nil, "my-label", "myContents")
	if err != nil {
		t.Errorf("%s", err)
	}
	if len(result.Nodes) != 1 {
		t.Errorf("Wrong amount of nodes with this label: %d", len(result.Nodes))
	}

	if result.Nodes[0].ObjectMeta.Name != "node" {
		t.Errorf("Wrong name of the node: %s", result.Nodes[0].ObjectMeta.Name)
	}
}

func TestRemovingLabel(t *testing.T) {
	nodesObjects := testingUtilities.CreateListOfNodes([]string{"node"})
	fakeClient := fake.NewSimpleClientset(nodesObjects...)
	SetNodeLabel(fakeClient, "node", []string{"my-label"}, []string{"myContents"})
	RemoveNodeLabel(fakeClient, "node", []string{"my-label"})
	result, err := node.GetNodeList(fakeClient, dataselect.NoDataSelect, nil)
	if err != nil {
		t.Errorf("Error while getting node list: %s", err)
	}
	if len(result.Nodes) != 1 {
		t.Errorf("Wrong amount of nodes: %d", len(result.Nodes))
	}
	if result.Nodes[0].ObjectMeta.Labels["my-label"] != "" {
		t.Errorf("Wrong contents of label: %s", result.Nodes[0].ObjectMeta.Labels["my-label"])
	}

}

func TestRemovingLabelFromNonExistentNodeThrowsError(t *testing.T) {
	nodesObjects := testingUtilities.CreateListOfNodes([]string{"node"})
	fakeClient := fake.NewSimpleClientset(nodesObjects...)
	err := RemoveNodeLabel(fakeClient, "node-which-doesn-t-exist", []string{"my-label"})
	if err == nil {
		t.Errorf("No error thrown")
	}
}

func TestAddingLabelToNonExistentNodeThrowsError(t *testing.T) {
	nodesObjects := testingUtilities.CreateListOfNodes([]string{"node"})
	fakeClient := fake.NewSimpleClientset(nodesObjects...)
	err := SetNodeLabel(fakeClient, "node-which-doesn-t-exist", []string{"my-label"}, []string{"myContents"})
	if err == nil {
		t.Errorf("No error thrown")
	}
}
