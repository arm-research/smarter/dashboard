package label

import (
	"context"
	"errors"
	"fmt"
	"log"

	v1 "k8s.io/api/core/v1"
	v1meta "k8s.io/apimachinery/pkg/apis/meta/v1"
	types "k8s.io/apimachinery/pkg/types"
	client "k8s.io/client-go/kubernetes"
)

type NodeList struct {
	Nodes []string `json:"nodesList"`
}

type LabelsCommand struct {
	LabelsNames    []string  `json:"labelsNames"`
	LabelsContents []string  `json:"labelsContents"`
	Nodes          *NodeList `json:"nodes"`
}

func convertLabelToJsonEntry(labelName string, labelContents string) string {
	if labelContents != "" {
		labelContents = fmt.Sprintf("\"%s\"", labelContents)
	} else {
		labelContents = `null`
	}
	return fmt.Sprintf("\"%s\":%s", labelName, labelContents)
}

func convertLabelsToJsonEntry(labelsNames []string, labelsContents []string) string {
	result := ""
	if labelsContents == nil {
		for i := 0; i < len(labelsNames); i++ {
			if i == 0 {
				result = result + convertLabelToJsonEntry(labelsNames[0], ``)
			} else {
				result = result + ", " + convertLabelToJsonEntry(labelsNames[i], ``)
			}
		}
	} else {
		if len(labelsNames) != len(labelsContents) {
			return ""
		}

		for i := 0; i < len(labelsNames); i++ {
			if i == 0 {
				result = result + convertLabelToJsonEntry(labelsNames[0], labelsContents[0])
			} else {
				result = result + ", " + convertLabelToJsonEntry(labelsNames[i], labelsContents[i])
			}
		}
	}
	log.Printf("%s", result)
	return result
}

// createPatch generates the byte slice containing JSON patch for the kubernetes control plane API
func createPatch(labelsName []string, labelsContents []string) []byte {
	return []byte(fmt.Sprintf("{\"metadata\":{\"labels\":{%s}}}", convertLabelsToJsonEntry(labelsName, labelsContents)))
}

func sendPatch(client client.Interface, nodeName string, patchBytesArray []byte) (*v1.Node, error) {
	return client.CoreV1().Nodes().Patch(
		context.TODO(),
		nodeName,
		types.MergePatchType,
		patchBytesArray,
		v1meta.PatchOptions{})
}

func SetNodeLabel(client client.Interface, nodeName string, labelsNames []string, labelsContents []string) error {
	patchBytesArray := createPatch(labelsNames, labelsContents)
	_, err := sendPatch(client, nodeName, patchBytesArray)

	if err != nil {
		return err
	}

	return nil

}

func SetMultipleNodesLabels(client client.Interface, command *LabelsCommand) error {
	var err error
	nodes := command.Nodes
	labelsNames := command.LabelsNames
	labelsContents := command.LabelsContents
	for _, currentNode := range nodes.Nodes {
		err = SetNodeLabel(client, currentNode, labelsNames, labelsContents)
		if err != nil {
			return err
		}
	}
	return nil
}

func RemoveNodeLabel(client client.Interface, nodeName string, labelsNames []string) error {
	patchBytesArray := createPatch(labelsNames, nil)
	_, err := sendPatch(client, nodeName, patchBytesArray)

	if err != nil {
		return errors.New(fmt.Sprintf("Couldn't change label of node: %s\n", err))
	}

	return nil
}

func RemoveMultipleNodesLabels(client client.Interface, command *LabelsCommand) error {
	var err error
	nodes := command.Nodes
	labelsNames := command.LabelsNames
	for _, currentNode := range nodes.Nodes {
		err = RemoveNodeLabel(client, currentNode, labelsNames)
		if err != nil {
			return err
		}
	}
	return nil
}
