package query

import (
	//"fmt"
	"fmt"
	"testing"

	"github.com/kubernetes/dashboard/src/app/backend/resource/dataselect"
	"github.com/kubernetes/dashboard/src/app/backend/resource/label"
	testingUtilities "github.com/kubernetes/dashboard/src/app/backend/testing"
	"k8s.io/client-go/kubernetes/fake"
)

func TestPrinting(t *testing.T) {
	nodesObjects := testingUtilities.CreateListOfNodes([]string{"node"})
	fakeClient := fake.NewSimpleClientset(nodesObjects...)
	label.SetNodeLabel(fakeClient, "node", []string{"my.label"}, []string{"myContents"})
	res, _ := Evaluate(fakeClient, dataselect.NoDataSelect, nil, "contents(\"my.label\")==\"myContents\"")
	fmt.Printf("Should be one: %d\n", res.ListMeta.TotalItems)
}
