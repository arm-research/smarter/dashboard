package query

import (
	"github.com/kubernetes/dashboard/src/app/backend/api"

	expr "github.com/antonmedv/expr"
	metricapi "github.com/kubernetes/dashboard/src/app/backend/integration/metric/api"
	"github.com/kubernetes/dashboard/src/app/backend/resource/dataselect"
	"github.com/kubernetes/dashboard/src/app/backend/resource/node"
	client "k8s.io/client-go/kubernetes"
	//"github.com/antonmedv/expr"
)

func accessLabels(labels map[string]string) func(string) string {
	return func(name string) string {
		return labels[name]
	}
}

func Evaluate(client client.Interface, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient, query string) (*node.NodeList, error) {
	nodeList, err := node.GetNodeList(client, dsQuery, metricClient)

	if err != nil {
		return nil, err
	}

	result := &node.NodeList{
		Nodes:    make([]node.Node, 0),
		ListMeta: api.ListMeta{TotalItems: 0},
	}

	for _, node := range nodeList.Nodes {
		env := map[string]interface{}{
			"contents": accessLabels(node.ObjectMeta.Labels),
		}
		out, err := expr.Eval(query, env)
		if err != nil {
			return nil, err
		}

		if out == true {
			result.Nodes = append(result.Nodes, node)
		}

	}
	result.ListMeta.TotalItems = len(result.Nodes)

	return result, nil
}
