package hierarchy

import (
	"context"
	"errors"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	metricapi "github.com/kubernetes/dashboard/src/app/backend/integration/metric/api"
	"github.com/kubernetes/dashboard/src/app/backend/resource/dataselect"
	"github.com/kubernetes/dashboard/src/app/backend/resource/label"
	"github.com/kubernetes/dashboard/src/app/backend/resource/node"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	client "k8s.io/client-go/kubernetes"
)

type HierarchyPath struct {
	HierarchyPath string `json:"hierarchyPath"`
}

type HierarchiesList struct {
	HierarchiesList []string `json:"hierarchiesList"`
}

type HierarchyEntry struct {
	Hierarchy string `json:"hierarchy"`
	Path      string `json:"path"`
}

type NodeHierarchyInformation struct {
	HierarchiesList []HierarchyEntry `json:"hierarchiesList"`
}

type HierarchyMultipleAdd struct {
	NodesList     []string `json:"nodesList"`
	HierarchyPath string   `json:"hierarchyPath"`
}

//Takes a string and validates it against regexp and common checks, returns corrected version
func validateString(input string) (string, error) {
	if input == "/" {
		return "/", nil
	}

	if strings.Contains(input, ".") {
		return "", errors.New("Cannot contain illegal separator")
	}

	//Last character is slash and it shouldn't be
	if input[len(input)-1:] == "/" {
		input = input[0:(len(input) - 1)]
	}

	if input[len(input)-1:] == "/" {
		input = input[0:(len(input) - 1)]
	}

	match, err := regexp.MatchString("^(?:[/](?:[a-z0-9A-Z]|(?:[a-z0-9A-Z](?:[a-z0-9A-Z]|[-]|[/]|[.])*[a-z0-9A-Z])))+$", input)
	if err != nil {
		return "", err
	}
	if match {
		return input, nil
	} else {
		return "", errors.New("Validation failure")
	}

}

//Takes a given path and makes it into the valid label, returns also depth of the path
func convertPath(path string) (string, int, error) {
	input, err := validateString(path)
	if err != nil {
		return "", 0, err
	}

	split := strings.Split(input, "/")
	var replaceString string
	var directoryLevel int

	//If getting the root - don't add "-" at the end and ask about label at depth 0
	if input == "/" {
		replaceString = "root"
		directoryLevel = 0
	} else {
		//otherwise prepare first bit of label contents
		replaceString = "root."
		directoryLevel = len(split) - 1
	}
	//Convert path to proper label value
	labelContents := strings.Replace(input, "/", replaceString, 1)
	labelContents = strings.Replace(labelContents, "/", ".", len(split)-1)
	return labelContents, directoryLevel, nil
}

func GetHierarchiesList(client client.Interface, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient) (*HierarchiesList, error) {

	//Get all nodes
	nodesList, err := node.GetNodeList(client, dsQuery, metricClient)
	if err != nil {
		return nil, err
	}

	//Dumb hashSet as go doesn't have one
	set := make(map[string]bool)

	//Go through nodes
	for _, node := range nodesList.Nodes {
		for labelName, labelVal := range node.ObjectMeta.Labels {
			//If we find "-hierarchy0" label with root value then we process the label
			match, matchErr := regexp.MatchString("^(?:[a-z0-9A-Z]|(?:[a-z0-9A-Z][a-z0-9A-Z-_]*[a-z0-9A-Z])).hierarchy0$", labelName)
			if matchErr != nil {
				return nil, matchErr
			}

			if match && labelVal == "root" {
				//Extract the hierarchy name
				hierarchyName := strings.Replace(labelName, ".hierarchy0", "", 1)
				//And add it to the set
				set[hierarchyName] = true
			}
		}
	}

	namesSlice := make([]string, 0)

	//Get all keys and put them in the lice
	for key, _ := range set {
		namesSlice = append(namesSlice, key)
	}

	//Return the slice encapsulated in return type
	return &HierarchiesList{HierarchiesList: namesSlice}, nil
}

func DeleteHierarchyFromNode(client client.Interface, nodeName string, hierarchyName string) error {
	// Run a query through k8s client
	node, err := client.CoreV1().Nodes().Get(context.TODO(), nodeName, metaV1.GetOptions{})
	if err != nil {
		log.Printf("Couldn't remove hierarchy label from node %s: %s\n", nodeName, err)
		return err
	}
	//Try to get the label showing the hierarchy depth
	labelName := fmt.Sprintf("%s.hierarchy.depth", hierarchyName)
	labelContents := node.Labels[labelName]
	if labelContents == "" {
		return nil
	}

	//Convert label to int in order to use it for iteration
	val, err := strconv.Atoi(labelContents)
	if err != nil {
		return errors.New(fmt.Sprintf("error converting string to int: %s", err))
	}

	//Remove all levels of hierarchy
	fmt.Printf("%d", val)
	for i := 0; i < val; i++ {
		label.RemoveNodeLabel(client, nodeName, []string{fmt.Sprintf("%s.hierarchy%d", hierarchyName, i)})
	}

	label.RemoveNodeLabel(client, nodeName, []string{fmt.Sprintf("%s.hierarchy.depth", hierarchyName)})
	return nil

}

func SetHierarchy(client client.Interface, nodeName string, hierarchyName string, hierarchyPath *HierarchyPath) error {
	validatedPath, err := validateString(hierarchyPath.HierarchyPath)
	if err != nil {
		return err
	}

	//First clean old labels
	err = DeleteHierarchyFromNode(client, nodeName, hierarchyName)
	if err != nil {
		return err
	}

	split := strings.Split(validatedPath, "/")
	var depthValue int
	if validatedPath == "/" {
		depthValue = 1
	} else {
		depthValue = len(split)
	}

	path := "root"
	err = label.SetNodeLabel(client, nodeName, []string{fmt.Sprintf("%s.hierarchy.depth", hierarchyName)}, []string{fmt.Sprintf("%d", depthValue)})
	if err != nil {
		return errors.New("Error happened when setting hierarchy depth")
	}

	for i := 0; i < depthValue; i++ {

		if i == 0 {
			err = label.SetNodeLabel(client, nodeName, []string{fmt.Sprintf("%s.hierarchy0", hierarchyName)}, []string{path})
			if err != nil {
				return errors.New("Error happened when setting root level hierarchy")
			}
		} else {
			folder := split[i]
			//Build path for consecutive labels
			path = fmt.Sprintf("%s.%s", path, folder)
			//Build label name
			labelName := fmt.Sprintf("%s.hierarchy%d", hierarchyName, i)
			err := label.SetNodeLabel(client, nodeName, []string{labelName}, []string{path})
			if err != nil {
				return errors.New(fmt.Sprintf("Error happened when setting hierarchy at depth %d to %s", i, hierarchyName))
			}
		}
	}
	return nil
}

func SetMultipleNodesHierarchy(client client.Interface, nodes []string, hierarchyName string, hierarchyPath string) error {

	var err error

	for _, currentNode := range nodes {

		err = SetHierarchy(client, currentNode, hierarchyName, &HierarchyPath{HierarchyPath: hierarchyPath})
		if err != nil {
			return err
		}
	}
	return nil
}

func DeleteMultipleNodesHierarchy(client client.Interface, nodes *label.NodeList, hierarchyName string) error {

	var err error

	for _, currentNode := range nodes.Nodes {

		err = DeleteHierarchyFromNode(client, currentNode, hierarchyName)
		if err != nil {
			return err
		}
	}
	return nil
}

func GetNodesListByHierarchy(client client.Interface, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient, hierarchyName string, hierarchyPath *HierarchyPath) (*node.NodeList, error) {
	labelContents, directoryLevel, err := convertPath(hierarchyPath.HierarchyPath)
	if err != nil {
		return nil, err
	}

	//Generate name of the level, which takes correct depth into account
	labelName := fmt.Sprintf("%s.hierarchy%d", hierarchyName, directoryLevel)

	//Use get node list functionality from resource/node package
	return node.GetNodeListByLabel(client, dsQuery, metricClient, labelName, labelContents)
}

func BuildTreeFromPath(client client.Interface, dsQuery *dataselect.DataSelectQuery, metricClient metricapi.MetricClient, hierarchyName string, hierarchyPath *HierarchyPath) (*HierarchyTree, error) {
	//Get all the nodes in the given hierarchy or deeper
	nodesList, err := GetNodesListByHierarchy(client, dsQuery, metricClient, hierarchyName, hierarchyPath)
	if err != nil {
		return nil, err
	}

	mountPoint, _, _ := convertPath(hierarchyPath.HierarchyPath)

	var returnTree *HierarchyTree = nil

	depthLabelName := fmt.Sprintf("%s.hierarchy.depth", hierarchyName)
	//Go through all the nodes in the resulting NodeList
	for _, currentNode := range nodesList.Nodes {
		depthString := currentNode.ObjectMeta.Labels[depthLabelName]
		if depthString == "" {
			continue
		} else {
			val, err := strconv.Atoi(depthString)
			if err != nil {
				return nil, errors.New(fmt.Sprintf("error converting string to int: %s", err))
			}

			highestDepthLabelName := fmt.Sprintf("%s.hierarchy%d", hierarchyName, val-1)
			pathLabel := currentNode.ObjectMeta.Labels[highestDepthLabelName]
			if pathLabel == "" {
				return nil, errors.New(fmt.Sprintf("%s is %s", highestDepthLabelName, pathLabel))
			}

			var isSelected bool = false

			res, ok := currentNode.ObjectMeta.Labels["arm-selection"]
			if ok {
				if res == "true" {
					isSelected = true
				}
			}

			//After passing all the checks add to constructed tree
			returnTree = AddNodeToTree(pathLabel, currentNode.ObjectMeta.Name, mountPoint, returnTree, isSelected)
		}
	}

	if returnTree == nil {
		return nil, errors.New("couldn't find that path")
	}
	CheckFolder(returnTree)
	//Return constructed tree
	return returnTree, nil
}

func GetHierarchiesListOfNode(client client.Interface, metricClient metricapi.MetricClient, name string, dsQuery *dataselect.DataSelectQuery) (*NodeHierarchyInformation, error) {
	node, err := node.GetNodeDetail(client, metricClient, name, dsQuery)
	if err != nil {
		return nil, err
	}

	result := make([]HierarchyEntry, 0)

	for labelName, labelVal := range node.ObjectMeta.Labels {
		//If we find "-hierarchy0" label with root value then we process the label
		match, matchErr := regexp.MatchString("^(?:[a-z0-9A-Z]|(?:[a-z0-9A-Z][a-z0-9A-Z-_]*[a-z0-9A-Z])).hierarchy.depth$", labelName)
		if matchErr != nil {
			return nil, matchErr
		}

		if match {
			val, err2 := strconv.Atoi(labelVal)
			if err2 != nil {
				return nil, fmt.Errorf("error converting string to int: %s", err)
			}
			//Extract the hierarchy name
			hierarchyName := strings.Replace(labelName, ".hierarchy.depth", "", 1)

			//Get top path
			pathLabelName := fmt.Sprintf("%s.hierarchy%d", hierarchyName, val-1)
			path, ok := node.ObjectMeta.Labels[pathLabelName]
			if !ok {
				return nil, fmt.Errorf("Couldnt retrieve label value: %s", pathLabelName)
			}
			if path == "root" {
				path = "/"
			} else {
				path = strings.Replace(path, "root", "", 1)
				path = strings.ReplaceAll(path, ".", "/")
			}
			result = append(result, HierarchyEntry{Hierarchy: hierarchyName, Path: path})
		}
	}
	return &NodeHierarchyInformation{HierarchiesList: result}, nil

}
