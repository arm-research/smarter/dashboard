package hierarchy

import (
	"fmt"
	"strings"
)

type HierarchyTree struct {
	Name     string                   `json:"name"`
	Selected bool                     `json:"selected"`
	Children map[string]HierarchyTree `json:"children"`
}

//Adds a node to the tree, takes label of the node, it's name and the root maunt point (in the form of label, not path)
func AddNodeToTree(highestLabel string, nodeName string, mountPoint string, oldTree *HierarchyTree, isSelected bool) *HierarchyTree {
	currentPointer := oldTree

	//If we have empty tree, create it
	if currentPointer == nil {
		currentPointer = &HierarchyTree{
			Name:     mountPoint,
			Selected: false,
			Children: make(map[string]HierarchyTree),
		}
		oldTree = currentPointer

	}

	// If the node is at mount point just add it and return
	if highestLabel == mountPoint {
		currentPointer.Children[nodeName] = HierarchyTree{
			Name:     nodeName,
			Selected: isSelected,
			Children: nil,
		}

		return oldTree
	} else {
		//Otherwise, split path into the mount point bit and the further subdirectories
		removeString := fmt.Sprintf("%s.", mountPoint)
		highestLabel = strings.Replace(highestLabel, removeString, "", 1)
	}

	split := strings.Split(highestLabel, ".")

	//Go down the tree for each of the folders
	for _, currentFolder := range split {
		res, ok := currentPointer.Children[currentFolder]
		if ok {
			currentPointer = &res
		} else {
			//If the given folder doesn't exist, then create the node and change the pointer
			newNode := HierarchyTree{
				Name:     currentFolder,
				Selected: false,
				Children: make(map[string]HierarchyTree),
			}
			currentPointer.Children[currentFolder] = newNode
			currentPointer = &newNode

		}
	}

	//Finally create the file info for a node, and put it into the folder we accessed
	currentPointer.Children[nodeName] = HierarchyTree{
		Name:     nodeName,
		Selected: isSelected,
		Children: nil,
	}

	return oldTree
}

//Marks folders if all they children have been visited
func CheckFolder(tree *HierarchyTree) bool {
	if tree.Children == nil {
		return (tree.Selected)
	} else {
		myArr := make([]string, 0)
		for name := range tree.Children {
			myArr = append(myArr, name)
		}
		result := true
		for _, name := range myArr {
			var child HierarchyTree = tree.Children[name]
			if !CheckFolder(&child) {
				tree.Selected = false
				result = false
			}
			tree.Children[name] = child
		}
		tree.Selected = result
		return result
	}
}
