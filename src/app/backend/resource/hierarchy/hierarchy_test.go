package hierarchy

import (
	"fmt"
	"testing"

	"github.com/kubernetes/dashboard/src/app/backend/resource/dataselect"
	"github.com/kubernetes/dashboard/src/app/backend/resource/node"
	client "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/kubernetes/fake"

	testingUtilities "github.com/kubernetes/dashboard/src/app/backend/testing"
)

// Abstraction for opeartions that can be performed using fake client interface
// Returns true if succeeds
type Performable interface {
	performOperation(client client.Interface) bool
}

// Abstraction for predicates that can be checked using fake client interface
type Testable interface {
	verify(client client.Interface) bool
}

// Put operation takes node and in given hierarchy it gives a path
type putOperation struct {
	nodeName      string
	hierarchyName string
	path          string
}

// Delete takes node and strips it off the information of given hierarchy
type deleteOperation struct {
	nodeName      string
	hierarchyName string
}

// Takes an operation which is expected to fail
type failableOperation struct {
	operationToFail Performable
}

//We ask about a node in a given hierarchy, which should respond with a correct path, with correct labels and depth
type returnPredicate struct {
	verificationHierarchy string
	verificationNode      string
	depth                 string
	levels                []string
}

type hierarchiesListPredicate struct {
	hierarchiesList []string
}

type nodeHierarchyListPredicate struct {
	nodeName    string
	hierarchies NodeHierarchyInformation
}

type failablePredicate struct {
	predicate Testable
}

//Encapsulates put opearation from the info from the given struct
func (operation *putOperation) performOperation(client client.Interface) bool {
	fmt.Printf("Performing put operation on node: %s in hierarchy: %s with path: %s \n", (*operation).nodeName, (*operation).hierarchyName, (*operation).path)
	err := SetHierarchy(client, (*operation).nodeName, (*operation).hierarchyName, &HierarchyPath{HierarchyPath: (*operation).path})
	if err != nil {
		return false
	}
	return true
}

//Encapsulates delete operation from the info from givens truct
func (operation *deleteOperation) performOperation(client client.Interface) bool {
	fmt.Printf("Performing delete operation on node: %s in hierarchy: %s\n", (*operation).nodeName, (*operation).hierarchyName)
	err := DeleteHierarchyFromNode(client, (*operation).nodeName, (*operation).hierarchyName)
	if err != nil {
		return false
	}
	return true
}

//Returns success if operation failed
func (operation *failableOperation) performOperation(client client.Interface) bool {
	return !operation.operationToFail.performOperation(client)
}

//Checking for negative predicate
func (currentPredicate *failablePredicate) verify(client client.Interface) bool {
	return !currentPredicate.predicate.verify(client)
}

func (currentPredicate *nodeHierarchyListPredicate) verify(client client.Interface) bool {
	result, err := GetHierarchiesListOfNode(client, nil, currentPredicate.nodeName, dataselect.NoDataSelect)
	if err != nil {
		return false
	}
	hashMap := make(map[string]string)
	for _, elem := range currentPredicate.hierarchies.HierarchiesList {
		hashMap[elem.Hierarchy] = elem.Path
	}

	if len(result.HierarchiesList) != len(currentPredicate.hierarchies.HierarchiesList) {
		fmt.Printf("Amount of hierarchies doesn't match")
		return false
	}

	for _, entry := range result.HierarchiesList {
		res, ok := hashMap[entry.Hierarchy]
		if ok {
			if res != entry.Path {
				fmt.Printf("wrong hierarchy path %s != %s\n", res, entry.Path)
				return false
			}
			continue
		}

		return false

	}
	return true
}

//Checking for list of hierarchies
func (currentPredicate *hierarchiesListPredicate) verify(client client.Interface) bool {
	returnedHierarchiesList, err := GetHierarchiesList(client, dataselect.NoDataSelect, nil)
	if err != nil {
		fmt.Printf("Error happened while retrieving hierarchies list")
		return false
	}

	list := returnedHierarchiesList.HierarchiesList

	if len(currentPredicate.hierarchiesList) != len(list) {
		fmt.Printf("Wrong amount of returned hierarchies")
		return false
	}

	set := make(map[string]bool)

	for _, name := range list {
		set[name] = true
	}

	for _, currentName := range currentPredicate.hierarchiesList {
		if !set[currentName] {
			fmt.Printf("Couldnt find hierarchy: %s", currentName)
		}
	}

	return true
}

//Encapsulates checking for returnPredicate
func (currentPredicate *returnPredicate) verify(client client.Interface) bool {
	fmt.Printf("Verification in hierarchy: %s\n", currentPredicate.verificationHierarchy)
	rootLabel := fmt.Sprintf("%s.hierarchy0", currentPredicate.verificationHierarchy)

	//Get all nodes containing given hierarchy name root
	result, err := node.GetNodeListByLabel(client, dataselect.NoDataSelect, nil, rootLabel, currentPredicate.levels[0])
	if err != nil {
		fmt.Printf("%s", err)
		return false
	}

	//Iterate over result nodes to get the one we are checking
	for _, resultNode := range result.Nodes {

		if currentPredicate.verificationNode != resultNode.ObjectMeta.Name {
			fmt.Printf("It's not the node we verify. Skipping...\n")
			continue
		}

		//Check verification string
		depthString := fmt.Sprintf("%s.hierarchy.depth", currentPredicate.verificationHierarchy)
		if resultNode.ObjectMeta.Labels[depthString] != currentPredicate.depth {
			fmt.Printf("Wrong hierarchy depth\n")
		}

		//Then go over all levels of hierarchy
		for index, currentLabelValue := range currentPredicate.levels {
			currentLabel := fmt.Sprintf("%s.hierarchy%d", currentPredicate.verificationHierarchy, index)
			if resultNode.ObjectMeta.Labels[currentLabel] != currentLabelValue {
				return false
			}
		}

	}
	return true
}

func TestValidateRemovesRedundandSlah(t *testing.T) {
	res, err := validateString("/a/")
	if err != nil {
		t.Errorf("%s", err)
	}
	if res != "/a" {
		t.Errorf("String incorrect")
	}
}

func TestValidateForRootPath(t *testing.T) {
	res, err := validateString("/")
	if err != nil {
		t.Errorf("%s", err)
	}
	if res != "/" {
		t.Errorf("String incorrect")
	}
}

func TestValidateForCorrectPath(t *testing.T) {
	res, err := validateString("/app/test")
	if err != nil {
		t.Errorf("%s", err)
	}
	if res != "/app/test" {
		t.Errorf("String incorrect")
	}
}

func TestAddingNodeToEmptyTree(t *testing.T) {
	var pointer *HierarchyTree = nil
	res := AddNodeToTree("root.nodes", "node", "root", pointer, false)
	if res == nil {
		t.Errorf("Tree wasn't created")
	}
	if res.Name != "root" {
		t.Errorf("Wrong root name")
	}
	if res.Children["nodes"].Name != "nodes" {
		t.Errorf("Wrong level 1 directory")
	}
	if res.Children["nodes"].Children["node"].Name != "node" {
		t.Errorf("Node not present")
	}
}

func TestAddingNodeToRoot(t *testing.T) {
	var pointer *HierarchyTree = nil
	res := AddNodeToTree("root", "node", "root", pointer, false)
	if res == nil {
		t.Errorf("Tree wasn't created")
	}
	if res.Name != "root" {
		t.Errorf("Wrong root name")
	}
	if res.Children["node"].Name != "node" {
		t.Errorf("Node not present")
	}
}

func TestAddingTwoNodes(t *testing.T) {
	var pointer *HierarchyTree = nil
	res := AddNodeToTree("root.cambridge", "node1", "root", pointer, false)
	res = AddNodeToTree("root.manchester.test", "node2", "root", res, false)

	if res == nil {
		t.Errorf("Tree wasn't created")
	}
	if res.Name != "root" {
		t.Errorf("Wrong root name")
	}

	if res.Children["cambridge"].Name != "cambridge" {
		t.Errorf("Folder not present")
	}

	if res.Children["manchester"].Name != "manchester" {
		t.Errorf("Folder not present")
	}

	if res.Children["cambridge"].Children["node1"].Name != "node1" {
		t.Errorf("Node not present")
	}

	if res.Children["manchester"].Children["test"].Name != "test" {
		t.Errorf("Folder not present")
	}

	if res.Children["manchester"].Children["test"].Children["node2"].Name != "node2" {
		t.Errorf("Node not present")
	}

}

//Runs the test suite
func TestNodeHierarchy(t *testing.T) {
	cases := []struct {
		testName   string
		nodes      []string      // create set of nodes with given names
		operations []Performable // then perform given opearations
		predicates []Testable    // then check given predicates
	}{
		{
			"Put op on two nodes, one with label reassigning by deletion",
			[]string{"testNode", "myNode", "othernode", "server3", "server4"},
			[]Performable{
				&putOperation{
					nodeName:      "myNode",
					hierarchyName: "user1",
					path:          "/no/not/this",
				},
				&putOperation{
					nodeName:      "testNode",
					hierarchyName: "user",
					path:          "/arm/nodes/cambridge",
				},
				&deleteOperation{
					nodeName:      "myNode",
					hierarchyName: "user1",
				},
				&putOperation{
					nodeName:      "myNode",
					hierarchyName: "user1",
					path:          "/the/correct/one",
				},
			},
			[]Testable{
				&nodeHierarchyListPredicate{
					nodeName: "testNode",
					hierarchies: NodeHierarchyInformation{
						HierarchiesList: []HierarchyEntry{
							HierarchyEntry{
								Hierarchy: "user",
								Path:      "/arm/nodes/cambridge",
							},
						},
					},
				},
				&nodeHierarchyListPredicate{
					nodeName: "myNode",
					hierarchies: NodeHierarchyInformation{
						HierarchiesList: []HierarchyEntry{
							HierarchyEntry{
								Hierarchy: "user1",
								Path:      "/the/correct/one",
							},
						},
					},
				},
				&returnPredicate{
					"user",
					"testNode",
					"4",
					[]string{"root", "root.arm", "root.arm.nodes", "root.arm.nodes.cambridge", "", ""},
				},
				&returnPredicate{
					"user1",
					"myNode",
					"4",
					[]string{"root", "root.the", "root.the.correct", "root.the.correct.one", "", ""},
				},
				&hierarchiesListPredicate{
					[]string{"user1", "user"},
				},
			},
		},
		{
			"Try putting a label to a node which doesnt exist",
			[]string{"node"},
			[]Performable{
				&failableOperation{
					operationToFail: &putOperation{
						nodeName:      "myNode1234",
						hierarchyName: "user1",
						path:          "/this/should/fail",
					},
				},
			},
			[]Testable{
				&failablePredicate{
					predicate: &hierarchiesListPredicate{
						[]string{"user1"},
					},
				},
			},
		},
		{
			"Try removing a label to a node which doesnt exist",
			[]string{"node"},
			[]Performable{
				&failableOperation{
					operationToFail: &deleteOperation{
						nodeName:      "myNode1234",
						hierarchyName: "user1",
					},
				},
			},
			[]Testable{
				&hierarchiesListPredicate{
					[]string{},
				},
			},
		},
		{
			"Simple label assigning to one node",
			[]string{"node"},
			[]Performable{
				&putOperation{
					nodeName:      "node",
					hierarchyName: "user1",
					path:          "/this/will/work",
				},
			},
			[]Testable{
				&nodeHierarchyListPredicate{
					nodeName: "node",
					hierarchies: NodeHierarchyInformation{
						HierarchiesList: []HierarchyEntry{
							HierarchyEntry{
								Hierarchy: "user1",
								Path:      "/this/will/work",
							},
						},
					},
				},
				&returnPredicate{
					"user1",
					"node",
					"4",
					[]string{"root", "root.this", "root.this.will", "root.this.will.work", "", ""},
				},
				&hierarchiesListPredicate{
					[]string{"user1"},
				},
			},
		},
		{
			"Simple label assigning to one node at root path",
			[]string{"node"},
			[]Performable{
				&putOperation{
					nodeName:      "node",
					hierarchyName: "user1",
					path:          "/",
				},
			},
			[]Testable{
				&nodeHierarchyListPredicate{
					nodeName: "node",
					hierarchies: NodeHierarchyInformation{
						HierarchiesList: []HierarchyEntry{
							HierarchyEntry{
								Hierarchy: "user1",
								Path:      "/",
							},
						},
					},
				},
				&returnPredicate{
					"user1",
					"node",
					"1",
					[]string{"root", ""},
				},
				&hierarchiesListPredicate{
					[]string{"user1"},
				},
			},
		},
		{
			"Setting hierarchy of the same node twice",
			[]string{"node"},
			[]Performable{
				&putOperation{
					nodeName:      "node",
					hierarchyName: "user1",
					path:          "/",
				},
				&putOperation{
					nodeName:      "node",
					hierarchyName: "user1",
					path:          "/my/hierarchy",
				},
			},
			[]Testable{
				&nodeHierarchyListPredicate{
					nodeName: "node",
					hierarchies: NodeHierarchyInformation{
						HierarchiesList: []HierarchyEntry{
							HierarchyEntry{
								Hierarchy: "user1",
								Path:      "/my/hierarchy",
							},
						},
					},
				},
				&returnPredicate{
					"user1",
					"node",
					"3",
					[]string{"root", "root.my", "root.my.hierarchy", ""},
				},
				&hierarchiesListPredicate{
					[]string{"user1"},
				},
			},
		},
	}

	for _, c := range cases {
		fmt.Printf("Running testcase: \"%s\"\n", c.testName)
		fmt.Printf("Creating nodes %s\n", c.nodes)

		//Create all the nodes with specified names
		nodesObjects := testingUtilities.CreateListOfNodes(c.nodes)
		fakeClient := fake.NewSimpleClientset(nodesObjects...)

		//Perform operations
		for _, operation := range c.operations {
			ret := operation.performOperation(fakeClient)
			if ret == false {
				t.Errorf("Operation didn't act as it was assumed")
			}
		}

		//Check predicates
		for _, currentPredicate := range c.predicates {
			if !currentPredicate.verify(fakeClient) {
				t.Errorf("Failed checking for predicate in the testcase: \"%s\"", c.testName)
			}

		}

	}
}
