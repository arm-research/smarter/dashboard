import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {HierarchyTree, HierarchyTreeMap} from '@api/backendapi';
import {ResourceService} from '../../../../common/services/resource/resource';
import {ActionbarService} from '../../../../common/services/global/actionbar';
import {Subscription} from 'rxjs/Subscription';
import {HttpClient} from '@angular/common/http';
import {findFlatIndexEntryPoint} from '@angular/compiler-cli/src/ngtsc/entry_point';

export interface Entry {
  isFolder: boolean;
  isSelected: boolean;
  name: string;
}

@Component({
  selector: 'kd-hierarchy-tree',
  templateUrl: './hierarchy-tree.component.html',
  styleUrls: ['./hierarchy-tree.component.scss'],
})
export class HierarchyTreeComponent implements OnInit {
  public hierarchyName: string;
  public hierarchyPath: string;
  private hierarchySubscription_: Subscription;
  private entries: Entry[] = [];
  public hierarchy: HierarchyTree;

  constructor(
    readonly http: HttpClient,
    private readonly hierarchies_: ResourceService<HierarchyTree>,
    private readonly actionbar_: ActionbarService,
    private readonly activatedRoute_: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.router.onSameUrlNavigation = 'reload';
    this.hierarchyName = this.activatedRoute_.snapshot.params.hierarchyName;
    this.hierarchyPath = this.activatedRoute_.snapshot.params.hierarchyPath;
    this.hierarchySubscription_ = this.hierarchies_
      .get('api/v1/hierarchy/tree-root/' + this.hierarchyName)
      .subscribe(newTree => {
        this.hierarchy = this.goDownTheTree(newTree);
        this.entries = this.makeEntries(this.hierarchy).sort((a, b) => {
          if (a.isFolder && b.isFolder) {
            return a.name > b.name ? 1 : -1;
          }
          if (a.isFolder) {
            return -1;
          }
          if (b.isFolder) {
            return 1;
          }
          return a.name > b.name ? 1 : -1;
        });
      });
  }

  goDownTheTree(givenTree: HierarchyTree): HierarchyTree {
    const directories = this.hierarchyPath.split('.');
    let currentTree = givenTree;
    if (directories.length > 1) {
      let index = 1;
      while (index < directories.length) {
        currentTree = currentTree.children[directories[index]];
        index++;
      }
    }
    return currentTree;
  }

  makeEntries(givenTree: HierarchyTree): Entry[] {
    const result: Entry[] = [];
    for (const [key, value] of Object.entries(givenTree.children)) {
      const isFolder = value.children !== null;
      result.push({isFolder: isFolder, name: key, isSelected: value.selected});
    }
    result.push({isFolder: true, name: '..', isSelected: false});
    return result;
  }

  flattenGivenDirectory(givenTree: HierarchyTree): string[] {
    let retVal: string[] = [];
    if (givenTree.children === null) {
      return [givenTree.name];
    }
    for (const childrenKey in givenTree.children) {
      retVal = retVal.concat(this.flattenGivenDirectory(givenTree.children[childrenKey]));
    }
    return retVal;
  }

  getEntries(): Entry[] {
    return this.entries;
  }

  getNumberOfItems(): number {
    return this.entries.length;
  }

  ngOnDestroy(): void {
    this.hierarchySubscription_.unsubscribe();
  }

  getLink(entry: Entry): string {
    if (entry.isFolder) {
      if (entry.name === '..') {
        if (this.hierarchyPath === 'root') {
          return '/hierarchy/';
        }
        const splitted = this.hierarchyPath.split('.');
        let previousPath = 'root';
        for (let i = 1; i < splitted.length - 1; i++) {
          previousPath += '.' + splitted[i];
        }
        return '/hierarchy/' + this.hierarchyName + '/' + previousPath;
      }
      return '/hierarchy/' + this.hierarchyName + '/' + this.hierarchyPath + '.' + entry.name;
    }
    return '/node/' + entry.name;
  }

  private getSubscription() {
    this.hierarchySubscription_ = this.hierarchies_
      .get('api/v1/hierarchy/tree-root/' + this.hierarchyName)
      .subscribe(newTree => {
        this.hierarchy = this.goDownTheTree(newTree);
        this.entries = this.makeEntries(this.hierarchy).sort((a, b) => {
          if (a.isFolder && b.isFolder) {
            return a.name > b.name ? 1 : -1;
          }
          if (a.isFolder) {
            return -1;
          }
          if (b.isFolder) {
            return 1;
          }
          return a.name > b.name ? 1 : -1;
        });
      });
  }

  update(entry: Entry) {
    if (!entry.isFolder) {
      this.http
        .put('api/v1/node/' + entry.name + '/label/arm-selection/' + entry.isSelected, {}, {responseType: 'text'})
        .subscribe(() => {
          this.getSubscription();
        });
    } else {
      const flattenedList: string[] = this.flattenGivenDirectory(this.hierarchy.children[entry.name]);
      this.http
        .put('api/v1/label-group/arm-selection/' + entry.isSelected, {nodesList: flattenedList}, {responseType: 'text'})
        .subscribe(() => {
          this.getSubscription();
        });
    }
  }
}
