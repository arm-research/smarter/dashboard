import {NgModule} from '@angular/core';

import {ComponentsModule} from '../../../common/components/module';
import {HierarchyRoutingModule} from './routing';
import {HierarchyListComponent} from './hierarchy-list/hierarchy-list.component';
import {MatListModule} from '@angular/material/list';
import {HierarchyTreeComponent} from './hierarchy-tree/hierarchy-tree.component';
import {SharedModule} from '../../../shared.module';

@NgModule({
  imports: [SharedModule, ComponentsModule, HierarchyRoutingModule, MatListModule],
  declarations: [HierarchyListComponent, HierarchyTreeComponent],
})
export class HierarchyModule {}
