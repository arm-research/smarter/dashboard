import {Component, Input, OnInit} from '@angular/core';
import {ResourceService} from '../../../../common/services/resource/resource';
import {HierarchiesList, NodeDetail} from '@api/backendapi';
import {ActionbarService} from '../../../../common/services/global/actionbar';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'kd-hierarchy-list',
  templateUrl: './hierarchy-list.component.html',
  styleUrls: ['./hierarchy-list.component.scss'],
})
export class HierarchyListComponent implements OnInit {
  @Input('api/v1/hierarchy')
  private hierarchies: string[] = [];
  private nodeSubscription_: Subscription;

  constructor(
    private readonly hierarchies_: ResourceService<HierarchiesList>,
    private readonly actionbar_: ActionbarService,
    private readonly activatedRoute_: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.nodeSubscription_ = this.hierarchies_
      .get('api/v1/hierarchy')
      .subscribe(
        newHierarchies =>
          (this.hierarchies = newHierarchies.hierarchiesList.sort((one: string, two: string) => (one > two ? 1 : -1))),
      );
  }

  getHierarchies() {
    return this.hierarchies;
  }

  getNumberOfItems() {
    return this.hierarchies.length;
  }

  ngOnDestroy(): void {
    this.nodeSubscription_.unsubscribe();
  }

  getLinkToHierarchy(hierarchy: string): string {
    return '/hierarchy/' + hierarchy + '/root';
  }
}
