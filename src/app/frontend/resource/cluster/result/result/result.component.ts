import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ResourceService} from '../../../../common/services/resource/resource';
import {Node, NodeList, Resource} from '@api/backendapi';
import {ActionbarService} from '../../../../common/services/global/actionbar';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'kd-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
})
export class ResultComponent implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    responseType: 'text' as 'text',
  };
  private nodes: Node[] = [];
  private nodesSubscription_: Subscription;
  constructor(
    readonly http: HttpClient,
    private readonly nodes_: ResourceService<NodeList>,
    private readonly actionbar_: ActionbarService,
    private readonly activatedRoute_: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.activatedRoute_.queryParams.subscribe(
      params =>
        (this.nodesSubscription_ = this.nodes_.get('api/v1/queryNodes?query=' + params['query']).subscribe(nodes => {
          this.nodes = nodes.nodes.sort((a, b) => {
            return a.objectMeta.name > b.objectMeta.name ? 1 : -1;
          });
        })),
    );
  }

  getNumberOfItems(): number {
    // eslint-disable-next-line eqeqeq
    return this.nodes.length;
  }

  getNodes(): Resource[] {
    return this.nodes;
  }

  getLink(entry: Resource): string {
    return '/node/' + entry.objectMeta.name;
  }
  private getSubscription() {
    this.activatedRoute_.queryParams.subscribe(
      params =>
        (this.nodesSubscription_ = this.nodes_.get('api/v1/queryNodes?query=' + params['query']).subscribe(nodes => {
          this.nodes = nodes.nodes.sort((a, b) => {
            return a.objectMeta.name > b.objectMeta.name ? 1 : -1;
          });
        })),
    );
  }

  update(state: boolean, entry: Resource) {
    this.http
      .put('api/v1/node/' + entry.objectMeta.name + '/label/arm-selection/' + state, {}, {responseType: 'text'})
      .subscribe(() => this.getSubscription());
  }

  isSelected(entry: Resource): boolean {
    return entry.objectMeta.labels['arm-selection'] === 'true';
  }

  updateAll(state: string) {
    this.http
      .put(
        'api/v1/label-group',
        {
          labelsNames: ['arm-selection'],
          labelsContents: [state],
          nodes: {nodesList: this.nodes.map(p => p.objectMeta.name)},
        },
        this.httpOptions,
      )
      .subscribe(() => this.getSubscription());
  }

  ngOnDestroy(): void {
    this.nodesSubscription_.unsubscribe();
  }
}
