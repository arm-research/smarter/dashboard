import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ResultRoutingModule} from './routing';
import {ComponentsModule} from '../../../common/components/module';
import {ResultComponent} from './result/result.component';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {FormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  declarations: [ResultComponent],
  imports: [
    CommonModule,
    ResultRoutingModule,
    ComponentsModule,
    MatIconModule,
    MatListModule,
    MatChipsModule,
    FormsModule,
    MatCheckboxModule,
  ],
})
export class ResultModule {}
