import {NgModule} from '@angular/core';
import {QueryviewComponent} from './queryview/queryview.component';

import {ComponentsModule} from '../../../common/components/module';
import {SharedModule} from '../../../shared.module';
import {QueryRoutingModule} from './routing';

@NgModule({
  declarations: [QueryviewComponent],
  imports: [SharedModule, ComponentsModule, QueryRoutingModule],
})
export class QueryModule {}
