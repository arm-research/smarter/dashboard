import {Component, OnInit} from '@angular/core';
import {ResourceService} from '../../../../common/services/resource/resource';
import {HierarchiesList} from '@api/backendapi';
import {ActionbarService} from '../../../../common/services/global/actionbar';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {GlobalSettingsService} from '../../../../common/services/global/globalsettings';

@Component({
  selector: 'kd-queryview',
  templateUrl: './queryview.component.html',
  styleUrls: ['./queryview.component.scss'],
})
export class QueryviewComponent implements OnInit {
  public myForm: FormGroup = new FormGroup({
    query: new FormControl('', [Validators.required]),
  });
  constructor(
    private readonly actionbar_: ActionbarService,
    private readonly activatedRoute_: ActivatedRoute,
    private readonly router_: Router,
    private readonly globalSettingsService_: GlobalSettingsService,
  ) {}

  ngOnInit(): void {}

  onSubmit() {
    this.globalSettingsService_.setLastQuery(this.myForm.value.query).subscribe(() => {
      this.router_.navigate(['/result'], {
        queryParams: {query: this.myForm.value.query},
      });
    });
  }

  loadOldQuery() {
    this.myForm.controls.query.setValue(this.globalSettingsService_.getLastQuery());
  }
}
