import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SelectedComponent} from './selection/selected/selected.component';
import {SelectionRoutingModule} from './routing';
import {ComponentsModule} from '../../../common/components/module';

@NgModule({
  declarations: [SelectedComponent],
  imports: [CommonModule, SelectionRoutingModule, ComponentsModule],
})
export class SelectionModule {}
