import {Component, ElementRef, Input, OnInit, SimpleChange, ViewChild} from '@angular/core';
import {GlobalSettingsService} from '../../../services/global/globalsettings';
import {MapInfoWindow, MapMarker, GoogleMap} from '@angular/google-maps';
import MapOptions = google.maps.MapOptions;
import MarkerOptions = google.maps.MarkerOptions;
import {HttpClient} from '@angular/common/http';
import {NodeDetail} from '@api/backendapi';

@Component({
  selector: 'kd-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  @ViewChild(GoogleMap, {static: false}) map: GoogleMap;
  public key: string;
  constructor(private readonly globalSettingsService_: GlobalSettingsService, readonly http: HttpClient) {}

  @Input() names: any;

  firstTimeCenter = false;
  mapLoading = false;
  mapLoaded = false;
  markers: MarkerOptions[] = [];
  center: google.maps.LatLngLiteral;
  // maxLat = -90.0;
  // maxLng = -180.0;
  // minLat = 90.0;
  // minLng = 180.0;

  ngOnChanges(changes: SimpleChange) {
    // @ts-ignore
    if (changes['names'].currentValue !== changes['names'].previousValue) {
      if (this.names !== null) {
        if (this.names !== [] && this.names[0] !== null && this.names.length > 0) {
          this.key = this.globalSettingsService_.getGoogleMapsKey();
          this.loadMap();
          this.names.forEach((name: string) => {
            this.http.get('/api/v1/node/' + name).subscribe((result: NodeDetail) => {
              let label = result.objectMeta.labels['arm-geography'];
              let currentLat;
              let currentLng;
              if (label !== '' && label.charAt(0) === 'G') {
                //Remove G
                label = label.substr(1, label.length - 1);

                let multiplier = 1;
                if (label.charAt(0) === '-') {
                  multiplier = -1;
                  label = label.substr(1, label.length - 1);
                }
                if (label.includes('--')) {
                  const mySplit = label.split('-', 3);
                  currentLat = multiplier * Number(mySplit[0]);
                  currentLng = Number(mySplit[2]) * -1;
                } else {
                  const mySplit = label.split('-', 2);
                  currentLat = multiplier * Number(mySplit[0]);
                  currentLng = Number(mySplit[1]);
                }
                if (!this.firstTimeCenter) {
                  this.center = {
                    lat: currentLat,
                    lng: currentLng,
                  };
                  this.firstTimeCenter = true;
                }
                // this.minLat = Math.min(this.minLat, currentLat);
                // this.maxLat = Math.max(this.maxLat, currentLat);
                // this.minLng = Math.min(this.minLng, currentLng);
                // this.maxLng = Math.max(this.maxLng, currentLng);
                this.markers.push({
                  position: {
                    lat: currentLat,
                    lng: currentLng,
                  },
                  label: {
                    color: 'black',
                    text: name,
                  },
                  title: name,
                });
              }
            });
          });
        }
      }
    }
  }

  loadMap() {
    if (this.mapLoaded || this.mapLoading) {
      return;
    }

    // One way of doing this: dynamically load a script tag.
    this.mapLoading = true;
    const mapsScript = document.createElement('script');
    mapsScript.setAttribute('async', '');
    mapsScript.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.key;
    mapsScript.addEventListener('load', () => {
      this.mapLoaded = true;
      this.mapLoading = false;
    });
    document.head.appendChild(mapsScript);
  }

  ngOnInit(): void {}
}
