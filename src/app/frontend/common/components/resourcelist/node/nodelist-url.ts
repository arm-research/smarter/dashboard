import {NodeListComponent} from './component';
import {ResourceService} from '../../../services/resource/resource';
import {DeploymentLabelDetail, NodeList, NodesList} from '@api/backendapi';
import {NotificationsService} from '../../../services/global/notifications';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input} from '@angular/core';
import {HierarchyDialog} from '../hierarchy/hierarchy.component';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {GlobalSettingsService} from '../../../services/global/globalsettings';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';

export interface AddLabelsDialogData {
  labelName: string;
  labelValue: string;
}

export interface DeleteLabelsDialogData {
  labelName: string;
  labelValue: string;
}

export interface AddHierarchyDialogData {
  hierarchyName: string;
  hierarchyPath: string;
}

export interface RemoveHierarchyDialogData {
  hierarchyName: string;
}

export interface DeployAppDialogData {
  appId: number;
  apps: string[];
}

@Component({
  selector: 'kd-node-list-url',
  templateUrl: './template-url.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeListUrl extends NodeListComponent {
  nodes: string[];
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
    responseType: 'text' as 'text',
  };

  viewMap: boolean;
  deploymentLabelDetail: DeploymentLabelDetail[];
  @Input() showAddLabel: boolean;
  constructor(
    readonly http: HttpClient,
    readonly childNode_: ResourceService<NodeList>,
    childNotifications: NotificationsService,
    childCdr: ChangeDetectorRef,
    public dialog: MatDialog,
    readonly settings_: GlobalSettingsService,
    readonly activatedRoute_: ActivatedRoute,
  ) {
    super(childNode_, childNotifications, childCdr);
    this.endpoint = '/api/v1/node/label/arm-selection/true';
  }

  clearSelection() {
    const dialogRef = this.dialog.open(ClearSelectionDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.viewMap = false;
        this.getNodesObservable().subscribe(p => {
          this.http
            .request('delete', '/api/v1/label-group/arm-selection', {
              body: {nodesList: p.nodes.map(n => n.objectMeta.name)},
            })
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  getNodesObservable(): Observable<NodeList> {
    // @ts-ignore
    return this.http.get(this.endpoint);
  }

  selectAll() {
    this.getNodesObservable().subscribe(p => {
      this.http
        .put(
          'api/v1/label-group',
          {
            labelsNames: ['arm-selection'],
            labelsContents: ['true'],
            nodes: {nodesList: p.nodes.map(n => n.objectMeta.name)},
          },
          this.httpOptions,
        )
        .subscribe(() => this.refresh());
    });
  }

  getAppsNames(): string[] {
    let result: string[] = [];
    this.deploymentLabelDetail = JSON.parse(this.settings_.getDeploymentLabelDetails());
    for (let i = 0; i < this.deploymentLabelDetail.length; i++) {
      result = result.concat(this.deploymentLabelDetail[i].name);
    }
    return result;
  }

  deployApp() {
    const dialogRef = this.dialog.open(DeployAppDialog, {
      width: '250px',
      data: {appId: 0, apps: this.getAppsNames()},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.appId !== null) {
        this.getNodesObservable().subscribe(p => {
          this.http
            .put(
              'api/v1/label-group',
              {
                labelsNames: this.deploymentLabelDetail[result.appId].labelsNames,
                labelsContents: this.deploymentLabelDetail[result.appId].labelsContents,
                nodes: {nodesList: p.nodes.map(n => n.objectMeta.name)},
              },
              this.httpOptions,
            )
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  undeployApp() {
    const dialogRef = this.dialog.open(DeployAppDialog, {
      width: '250px',
      data: {appId: 0, apps: this.getAppsNames()},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.appId !== null) {
        this.getNodesObservable().subscribe(p => {
          this.http
            .request('delete', 'api/v1/label-group', {
              body: {
                labelsNames: this.deploymentLabelDetail[result.appId].labelsNames,
                labelsContents: null,
                nodes: {nodesList: p.nodes.map(n => n.objectMeta.name)},
              },
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
              }),
              responseType: 'text' as 'text',
            })
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  addLabels() {
    const dialogRef = this.dialog.open(AddLabelsDialog, {
      width: '250px',
      data: {labelName: '', labelValue: ''},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.labelValue !== '' && result.labelName !== '') {
        this.getNodesObservable().subscribe(p => {
          this.http
            .put(
              'api/v1/label-group',
              {
                labelsNames: [result.labelName],
                labelsContents: [result.labelValue],
                nodes: {nodesList: p.nodes.map(n => n.objectMeta.name)},
              },
              this.httpOptions,
            )
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  deleteLabels() {
    const dialogRef = this.dialog.open(DeleteLabelsDialog, {
      width: '250px',
      data: {labelName: ''},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.labelValue !== '') {
        this.getNodesObservable().subscribe(p => {
          this.http
            .request('delete', '/api/v1/label-group', {
              body: {
                labelsNames: [result.labelName],
                labelsContents: null,
                nodes: {nodesList: p.nodes.map(n => n.objectMeta.name)},
              },
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
              }),
              responseType: 'text' as 'text',
            })
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  addHierarchy() {
    const dialogRef = this.dialog.open(AddHierarchyDialog, {
      width: '250px',
      data: {hierarchyName: '', hierarchyPath: ''},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.hierarchyName !== '' && result.hierarchyPath !== '') {
        this.getNodesObservable().subscribe(p => {
          this.http
            .put(
              'api/v1/hierarchy-group/' + result.hierarchyName,
              {nodesList: p.nodes.map(n => n.objectMeta.name), hierarchyPath: result.hierarchyPath},
              this.httpOptions,
            )
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  removeHierarchy() {
    const dialogRef = this.dialog.open(RemoveHierarchyDialog, {
      width: '250px',
      data: {hierarchyName: ''},
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.hierarchyName !== '') {
        this.getNodesObservable().subscribe(p => {
          this.http
            .request('delete', '/api/v1/hierarchy-group/' + result.hierarchyName, {
              body: {nodesList: p.nodes.map(n => n.objectMeta.name)},
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
              }),
              responseType: 'text' as 'text',
            })
            .subscribe(() => this.refresh());
        });
      }
    });
  }

  openMap() {
    this.getNodesObservable().subscribe(p => {
      this.nodes = p.nodes.map(n => n.objectMeta.name);
      this.viewMap = !this.viewMap;
    });
  }
}

@Component({
  selector: 'ClearSelectionDialog',
  templateUrl: './clear-selection-dialog.html',
})
export class ClearSelectionDialog {}

@Component({
  selector: 'AddLabelsDialog',
  templateUrl: './add-labels-dialog.html',
})
export class AddLabelsDialog {
  constructor(
    public dialogRef: MatDialogRef<AddLabelsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: AddLabelsDialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'AddLabelsDialog',
  templateUrl: './delete-labels-dialog.html',
})
export class DeleteLabelsDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteLabelsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DeleteLabelsDialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'AddHierarchyDialog',
  templateUrl: './add-hierarchy-dialog.html',
})
export class AddHierarchyDialog {
  constructor(
    public dialogRef: MatDialogRef<AddHierarchyDialog>,
    @Inject(MAT_DIALOG_DATA) public data: AddHierarchyDialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'RemoveHierarchyDialog',
  templateUrl: './remove-hierarchy-dialog.html',
})
export class RemoveHierarchyDialog {
  constructor(
    public dialogRef: MatDialogRef<RemoveHierarchyDialog>,
    @Inject(MAT_DIALOG_DATA) public data: RemoveHierarchyDialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}

@Component({
  selector: 'DeployAppDialog',
  templateUrl: './deploy-app-dialog.html',
})
export class DeployAppDialog {
  constructor(
    public dialogRef: MatDialogRef<DeployAppDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DeployAppDialogData,
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
