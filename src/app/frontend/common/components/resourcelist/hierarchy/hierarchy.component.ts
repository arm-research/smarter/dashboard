import {Component, Input, OnInit, SimpleChange} from '@angular/core';
import {ResourceService} from '../../../services/resource/resource';
import {HierarchyEntry, HierarchyPath, NodeHierarchyInformation} from '@api/backendapi';
import {ActionbarService} from '../../../services/global/actionbar';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
  responseType: 'text' as 'text',
};

@Component({
  selector: 'kd-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss'],
})
export class HierarchyComponent implements OnInit {
  @Input() nodeName: any;
  private hierarchies: HierarchyEntry[];
  private hierarchySubscription_: Subscription;
  public myForm: FormGroup = new FormGroup({
    hierarchy: new FormControl('', [Validators.required]),
    path: new FormControl('', [Validators.required]),
  });
  constructor(
    readonly http: HttpClient,
    private readonly hierarchies_: ResourceService<NodeHierarchyInformation>,
    private readonly actionbar_: ActionbarService,
    private readonly activatedRoute_: ActivatedRoute,
    public dialog: MatDialog,
  ) {}

  ngOnChanges(changes: SimpleChange) {
    // @ts-ignore
    if (changes['nodeName']) {
      if (this.nodeName !== null) {
        this.hierarchySubscription_ = this.hierarchies_
          .get('api/v1/hierarchy/list/' + this.nodeName)
          .subscribe(
            newHierarchies =>
              (this.hierarchies = newHierarchies.hierarchiesList.sort((one: HierarchyEntry, two: HierarchyEntry) =>
                one.hierarchy > two.hierarchy ? 1 : -1,
              )),
          );
      }
    }
  }

  ngOnInit(): void {}

  removeFromHierarchy(hierarchyName: string) {
    const dialogRef = this.dialog.open(HierarchyDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.http
          .delete('api/v1/hierarchy/' + this.nodeName + '/' + hierarchyName, {responseType: 'text'})
          .subscribe(() =>
            this.hierarchies_
              .get('api/v1/hierarchy/list/' + this.nodeName)
              .subscribe(
                newHierarchies =>
                  (this.hierarchies = newHierarchies.hierarchiesList.sort((one: HierarchyEntry, two: HierarchyEntry) =>
                    one.hierarchy > two.hierarchy ? 1 : -1,
                  )),
              ),
          );
      }
    });
  }

  getHierarchies(): HierarchyEntry[] {
    return this.hierarchies;
  }

  onSubmit() {
    const path = {} as HierarchyPath;
    path.hierarchyPath = this.myForm.value.path;
    // @ts-ignore
    this.http
      .put('api/v1/hierarchy/' + this.nodeName + '/' + this.myForm.value.hierarchy, path, httpOptions)
      .subscribe(() =>
        this.hierarchies_
          .get('api/v1/hierarchy/list/' + this.nodeName)
          .subscribe(
            newHierarchies =>
              (this.hierarchies = newHierarchies.hierarchiesList.sort((one: HierarchyEntry, two: HierarchyEntry) =>
                one.hierarchy > two.hierarchy ? 1 : -1,
              )),
          ),
      );

    // Do somthing
    // You can get the values in the forms like this: this.myForm.value
  }

  ngOnDestroy(): void {
    this.hierarchySubscription_.unsubscribe();
  }
}

@Component({
  selector: 'HierarchyDialog',
  templateUrl: './hierarchy.dialog.html',
})
export class HierarchyDialog {}
