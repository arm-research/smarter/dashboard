## This week (7-11 septmeber):
    - debug and fix map
    - create code for saving queries
    - start doing frontend for queries

## Next week (14-18 septmeber)
    - finish frontend for queries
    - do backend for queries
    - prepare the presentation and rehearse it

## Last week (21-24 septmeber)
    - give the presentation
    - tests and debugging
    - finish documentation