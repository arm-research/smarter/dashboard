**Remove given node from a given hierarchy**
----
   Delets given node from a given hierarchy

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleDeleteNodeHierarchy` - handles endpoint by calling `hierarchy.DeleteHierarchyFromNode`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.DeleteHierarchyFromNode`

* **URL**

  /hierarchy/{node}/{name}

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `node=[string]`
   `name=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 OK <br />


* **Error Response:**

   **Code:** 404 NOT FOUND<br />
   **Content:** `nodes "pop-oss" not found`



* **Tests**

  - [hierarchy_test.go](/src/app/backend/resource/hierarchy/hierarchy_test.go)
  - [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)

* **TODOS**



 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->