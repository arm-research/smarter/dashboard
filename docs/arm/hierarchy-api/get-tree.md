**Get a tree starting from a given directory**
----
   Gets a list of all nodes being in given hierarchy under given path. It returns a [`hierarchy.HierarchyTree`](/src/app/backend/resource/hierarchy/hierary_tree.go) data structure. Consumes [`hierarchy.HierarchyPath`](/src/app/backend/resource/hierarchy/hierarchy.go) data structure.

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleGetHierarchyTree` - handles endpoint by calling `hierarchy.BuildTreeFromPath`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.BuildTreeFromPath`
  - [`src/app/backend/resource/hierarchy/hierarchy_tree.go`](/src/app/backend/resource/hierarchy/hierarchy_tree.go)
    - `hierarchy.AddNodeToTree`

* **URL**

  /hierarchy/tree/{name}

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `name=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  Consumes JSON containing `hierarchy.HierarchyPath`, example:
  ```javascript
    {"HierarchyPath": "/raspberryPis/camera/cambridge/building1"}
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```javascript
    {
        "name": "root",
        "children": {
            "nodes": {
                "name": "nodes",
                "children": {
                    "cambridge": {
                        "name": "cambridge",
                        "children": {
                            "pop-os": {
                                "name": "pop-os",
                                "children": null
                            }
                        }
                    }
                }
            }
        }
    }
    ```

* **Error Response:**

  * **Code:** 500<br />
    **Content:** 
    ```
    couldn't find that path
    ```

* **Tests**

  - [hierarchy_test.go](/src/app/backend/resource/hierarchy/hierarchy_test.go)
  - [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)

* **TODOS**


 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->