**Get a list of hierarchies of a given node with the paths inside each of the hierarchies**
----
   Gets a list of all hierarchies of a given path. It returns a [`hierarchy.NodeHierarchyInformation`](/src/app/backend/resource/hierarchy/hierary.go) data structure. 

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleGetHierarchiesList` - handles endpoint by calling `hierarchy.GetHierarchiesList`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.GetHierarchiesList`


* **URL**

  /hierarchy/list/{node}

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
     
   `node=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```javascript
    {
        "hierarchiesList": [
            "arm",
            "arm2"
        ]
    }
    ```


* **Tests**

  - [hierarchy_test.go](/src/app/backend/resource/hierarchy/hierarchy_test.go)
  - [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)
  
* **TODOS**


 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->