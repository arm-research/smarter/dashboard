**Assign node to a given path in a given hierarchy**
----
   Assigns given node to a given path in a given hierarchy. Consumes [`hierarchy.HierarchyPath`](/src/app/backend/resource/hierarchy/hierarchy.go) data structure.

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleSetNodeHierarchy` - handles endpoint by calling `hierarchy.SetHierarchy`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.SetHierarchy`

* **URL**

  /hierarchy/{node}/{name}

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `node[string]`
   `name=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  Consumes JSON containing `hierarchy.HierarchyPath`, example:
  ```javascript
    {"HierarchyPath": "/raspberryPis/camera/cambridge/building1"}
  ```

* **Success Response:**

  * **Code:** 201 CREATED<br />

* **Error Response:**

   **Code:** 404 NOT FOUND<br />
   **Content:** `nodes "pop-oss" not found`


* **Tests**

  - [hierarchy_test.go](/src/app/backend/resource/hierarchy/hierarchy_test.go)
  - [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)

* **TODOS**

 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->