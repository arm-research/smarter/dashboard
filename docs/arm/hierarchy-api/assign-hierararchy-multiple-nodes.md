**Assign nodes to a given path in a given hierarchy**
----
   Assigns given list of nodes to a given path in a given hierarchy. Consumes [`hierarchy.HierarchyMultipleAdd`](/src/app/backend/resource/hierarchy/hierarchy.go) data structure.

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleSetMultipleNodesHierarchy` - handles endpoint by calling `hierarchy.SetMultipleNodesHierarchy`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.SetMultipleNodesHierarchy`

* **URL**

  /hierarchy-group/{name}

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**

   `name=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  Consumes JSON containing `hierarchy.HierarchyMultipleAdd`.


* **Success Response:**

  * **Code:** 201 CREATED<br />

* **Error Response:**

   **Code:** 404 NOT FOUND<br />
   **Content:** `nodes "pop-oss" not found`

 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->