**Get a list of all hierarchies**
----
   Gets a list of all hierarchies stored in cluster. It returns a [`hierarchy.HierarchiesList`](/src/app/backend/resource/hierarchy/hierary.go) data structure. 

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleGetNodeHierarchyInformation` - handles endpoint by calling `hierarchy.GetHierarchiesListOfNode`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.GetHierarchiesListOfNode`


* **URL**

  /hierarchy

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
    None

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```javascript
    {
      "HierarchiesList": [
        {
            "Hierarchy": "arm",
            "Path": "/nodes/cambridge"
        },
        {
            "Hierarchy": "user",
            "Path": "/"
        }
      ]
    }
    ```


* **Tests**

  - [hierarchy_test.go](/src/app/backend/resource/hierarchy/hierarchy_test.go)
  - [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)
  
* **TODOS**


 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->