**Remove nodes from a given hierarchy**
----
   Takes a list of nodes and removes them from given hierarchy. Consumes [`label.NodeList`](/src/app/backend/resource/label/label.go) data structure.

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleDeleteMultipleNodesHierarchy` - handles endpoint by calling `hierarchy.DeleteMultipleNodesHierarchy`
  - [`src/app/backend/resource/hierarchy/hierarchy.go`](/src/app/backend/resource/hierarchy/hierarchy.go)
    - `hierarchy.DeleteMultipleNodesHierarchy`

* **URL**

  /hierarchy-group/{name}

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**

   `name=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  Consumes JSON containing `label.NodeList`.


* **Success Response:**

  * **Code:** 201 CREATED<br />

* **Error Response:**

   **Code:** 404 NOT FOUND<br />
   **Content:** `nodes "pop-oss" not found`

 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->