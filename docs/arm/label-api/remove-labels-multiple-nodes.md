**Assign multiple label to multiple nodes**
----
  Takes a node list from `label.LabelsCommand` structure and remvoes all given labels from the given nodes

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleDeleteMultipleLabels` - handles endpoint by calling `label.RemoveMultipleNodesLabels`
  - [`src/app/backend/resource/label/label.go`](/src/app/backend/resource/label/label.go)
    - `label.RemoveMultipleNodesLabels`

* **URL**

  /label-group

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**

* **Data Params**

  `label.LabelsCommand`  structure in JSON format

* **Success Response:**

  * **Code:** 201 <br />
    **Content:** ``

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `nodes "pop-oss" not found`

  OR

  * **Code:** 422 UNPROCESSABLE ENTITY<br />
  **Content:** `Node "pop-os" is invalid: metadata.labels: Invalid value: "-smarter-label": name part must consist of alphanumeric characters, '-', '_' or '.', and must start and end with an alphanumeric character (e.g. 'MyName',  or 'my.name',  or '123-abc', regex used for validation is '([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]')`




 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->