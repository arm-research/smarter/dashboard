**Query nodes by their labels contents**
----
  Quries the nodes by their label contents. Queries are eveluated using the [Expr](https://github.com/antonmedv/expr) execution engine. Expression is evaluated for each node separately. If it evaluates to true then the node is in the output NodeList. Access to label contents is done throguh `contents()` function, which takes a string (label name) and returns a string (label contents).




* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleQueryNodeList` - handles endpoint by calling `query.Evaluate`
  - [`src/app/backend/resource/label/label.go`](/src/app/backend/resource/query/query.go)
    - `query.Evaluate`

* **URL**

  /queryNodes

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 

   `query=[string]`


* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```javascript

        {
        "listMeta": {
            "totalItems": 1
        },
        "nodes": [
            {
                "objectMeta": {
                    "name": "pop-os",
                    "labels": {
                        "arm-hierarchy-depth": "5",
                        "arm-hierarchy0": "root",
                        "arm-hierarchy1": "root-raspberryPis",
                        "arm-hierarchy2": "root-raspberryPis-camera",
                        "arm-hierarchy3": "root-raspberryPis-camera-cambridge",
                        "arm-hierarchy4": "root-raspberryPis-camera-cambridge-building1",
                        "beta.kubernetes.io/arch": "amd64",
                        "beta.kubernetes.io/instance-type": "k3s",
                        "beta.kubernetes.io/os": "linux",
                        "k3s.io/hostname": "pop-os",
                        "k3s.io/internal-ip": "192.168.1.22",
                        "kubernetes.io/arch": "amd64",
                        "kubernetes.io/hostname": "pop-os",
                        "kubernetes.io/os": "linux",
                        "node-role.kubernetes.io/master": "true",
                        "node.kubernetes.io/instance-type": "k3s"
                    },
                    "annotations": {
                        "flannel.alpha.coreos.com/backend-data": "{\"VtepMAC\":\"e2:ce:7b:29:de:7f\"}",
                        "flannel.alpha.coreos.com/backend-type": "vxlan",
                        "flannel.alpha.coreos.com/kube-subnet-manager": "true",
                        "flannel.alpha.coreos.com/public-ip": "192.168.1.22",
                        "k3s.io/node-args": "[\"server\"]",
                        "k3s.io/node-config-hash": "YLEKFGBK7CS7SJR3XG3MNMKXU3TI5IUS3NMZQMZ7FPQ7AFXAN4DQ====",
                        "k3s.io/node-env": "{\"K3S_DATA_DIR\":\"/var/lib/rancher/k3s/data/3a8d3d90c0ac3531edbdbde77ce4a85062f4af8865b98cedc30ea730715d9d48\"}",
                        "node.alpha.kubernetes.io/ttl": "0",
                        "volumes.kubernetes.io/controller-managed-attach-detach": "true"
                    },
                    "creationTimestamp": "2020-07-15T10:04:05Z",
                    "uid": "5cfde9f7-350b-4d75-ae30-ad05b7b8f8b8"
                },
                "typeMeta": {
                    "kind": "node"
                },
                "ready": "True",
                "allocatedResources": {
                    "cpuRequests": 100,
                    "cpuRequestsFraction": 1.25,
                    "cpuLimits": 0,
                    "cpuLimitsFraction": 0,
                    "cpuCapacity": 8000,
                    "memoryRequests": 73400320,
                    "memoryRequestsFraction": 0.21986483710984728,
                    "memoryLimits": 178257920,
                    "memoryLimitsFraction": 0.5339574615524862,
                    "memoryCapacity": 33384292352,
                    "allocatedPods": 5,
                    "podCapacity": 110,
                    "podFraction": 4.545454545454546
                }
            }
        ],
        "cumulativeMetrics": [],
        "errors": []
    }
    ```

* **Error Response:**

  * **Code:** 200<br />
    **Content:** 
    ```javascript 
    {
        "listMeta": {
            "totalItems": 0
        },
        "nodes": [],
        "cumulativeMetrics": [],
        "errors": []
    }
    ```

 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->