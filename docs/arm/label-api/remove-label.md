**Remove label**
----
  Removes given label from a given node. 

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleDeleteLabel` - handles endpoint by calling `label.RemoveNodeLabel`
  - [`src/app/backend/resource/label/label.go`](/src/app/backend/resource/label/label.go)
    - `label.RemoveNodeLabel`

* **URL**

  /node/{node}/label/{labelName}

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `node=[string]`
   `labelName=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** ``

* **Error Response:**

  * **Code:** 500 INTERNAL SERVER ERROR <br />
    **Content:** `Couldn't change label of  node: nodes "pop-oss" not found`

* **Tests**

  * [label_test.go](src/app/backend/resource/label/label_test.go)
  * [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)

* **TODOS**



 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->