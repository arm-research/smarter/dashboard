**Remove label from multiple nodes**
----
  Takes a node list from `label.NodeList` structure and removes label with a given name from each of those nodes

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handleDeleteLabels` - handles endpoint by calling `label.RemoveMultipleNodesLabels`
  - [`src/app/backend/resource/label/label.go`](/src/app/backend/resource/label/label.go)
    - `label.RemoveMultipleNodesLabels`

* **URL**

  /label-group/{labelName}

* **Method:**

  `DELETE`
  
*  **URL Params**

   **Required:**
 
   `labelName=[string]`
   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  `label.NodeList` structure in JSON format

* **Success Response:**

  * **Code:** 201 <br />
    **Content:** ``

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `nodes "pop-oss" not found`
