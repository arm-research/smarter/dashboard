**Assign label**
----
  Assigns label to a given node. 

* **Location in codebase**

  - [`src/app/backend/handler/apihandler.go`](/src/app/backend/handler/apihandler.go)
    - `apiHandler.handlePutLabel` - handles endpoint by calling `label.SetNodeLabel`
  - [`src/app/backend/resource/label/label.go`](/src/app/backend/resource/label/label.go)
    - `label.SetNodeLabel`

* **URL**

  /node/{node}/label/{labelName}/{labelContents}

* **Method:**

  `PUT`
  
*  **URL Params**

   **Required:**
 
   `node=[string]`
   `labelName=[string]`
   `labelContents=[string]`

   Note that contents of those strings must be valid names that can be used by kubernetes

* **Data Params**

  None

* **Success Response:**

  * **Code:** 201 <br />
    **Content:** ``

* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `nodes "pop-oss" not found`

  OR

  * **Code:** 422 UNPROCESSABLE ENTITY<br />
  **Content:** `Node "pop-os" is invalid: metadata.labels: Invalid value: "-smarter-label": name part must consist of alphanumeric characters, '-', '_' or '.', and must start and end with an alphanumeric character (e.g. 'MyName',  or 'my.name',  or '123-abc', regex used for validation is '([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9]')`

* **Tests**

  * [label_test.go](src/app/backend/resource/label/label_test.go)
  * [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)

* **TODOS**



 
<!-- * **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ error : "User doesn't exist" }`

  OR

  * **Code:** 401 UNAUTHORIZED <br />
    **Content:** `{ error : "You are unauthorized to make this request." }` -->

<!-- * **Sample Call:** -->

  <!-- ```javascript
    $.ajax({
      url: "/users/1",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ``` -->