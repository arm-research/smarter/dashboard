# Changes to dashboard done in ARM SMARTER fork 

## Overview

Changes done to the dashboard, as part of the SMARTER project of ARM Research are intended for providing functionality of hierarchical grouping of nodes, querying the nodes by their label contents, performing group operations on nodes as well as manipulating labels with their semantics (eg. providing geography tagging functionality).


## Labels API
The lowest level of the solution is the CRUD API for adding and removing the labels to nodes and quering the nodes by the values of the albels.
### HTTP endpoints
* [Assign label to a node](label-api/assign-label.md)
* [Remove given label from a node](label-api/remove-label.md)
* [Get a lists of all nodes with a given label having given value](label-api/get-list.md)
* [Query the nodes by their labels contents](label-api/query.md)
* [Add label to multiple nodes](label-api/add-label-multiple-nodes.md)
* [Remove label from multiple nodes](label-api/remove-label-multiple-nodes.md)
* [Add multiple labels to multiple nodes](label-api/add-labels-multiple-nodes.md)


## Hierarchy API

### Overview of the idea
Hierarchy functionality is implemented on top of label API. The idea was influenced by filesystem abstraction - folders, directories and root. 

However, contary to filesystems - one object can be a member of **multiple hierarchies** (each hierarchy is a separate tree-like structure) so for example:
- `node1` can be a member of `buildings` hierarchy, where it has path `/cambridge/arm/building1/floor2` 
- simultenously `node1` can be a member of `devices` hierarchy, where it has path `/armCpu/raspberryPis/withCamera`

The other difference between filesystem and hierarchy system is the fact that hierarchy doesn't support relative paths.

### Correct format of the path
In exposed API can only have alphanumeric directory names and are separated using `/` character - charcter `.` is strictly not allowed. Each path starts with a slash for a root directory, and `/` cannot be the last character of the path.

Correct path examples:
- `/`
- `/cambridge`
- `/cambridge0/folder1`

### Semantics of the labels
Kubernetes labels cannot start with non-alphanumeric character and `/` cannot be used as a character. Therefore, first `/` is transformed into `root` and then `.` is used as a separator

Transformation example:
- `/` is converted into `root`
- `/cambridge` is converted into `root.cambridge`
- `/cambridge0/folder1` is converted into `root.cambridge0.folder1`

If a node is in a subfolder, it also is in the parent folder, therefore there is one label per each of the levels of depth in filesystem. So, for example: if a node in one of the hierarchies is in path `/cambridge0/folder1`, then it contains following annotations:
- `root`
- `root.cambridge0`
- `root.cambridge0.folder1`

#### Label names
In kubernetes each label must have a name. To distinguish between multiple hierarchies each of the labels is prefixed with a hierarchy name and `.` symbol. 

As for the folders annotations, the postfix contains `hierarchy` and concatenated number showing the folder depth. For example:
If a node is in hierarchy `arm` and is stored in path `/cambridge0/folder1`, then it will contain following labels:
- `arm.hierarchy0: root`
- `arm.hierarchy1: root.cambridge0`
- `arm.hierarchy2: root.cambridge0.folder1`

Additionally, for easier management of multiple labels, there is an additional label showing the depth of the most nested folder, so the full set of labels in example shown before will be:
- `arm.hierarchy-depth: 3`
- `arm.hierarchy0: root`
- `arm.hierarchy1: root.cambridge0`
- `arm.hierarchy2: root.cambridge0.folder1`

#### Reasoning about the labels
The depth cannot be 0, because at lowest a node can be stored at root so the depth would be 1 in such case.

The easiest way to check if a `node1` is in `arm` hierarchy is to query all the nodes containing label `arm.hierarchy.0` with value `root` and then iterate over returned `NodeList` to check whether it contains `node1`

### HTTP endpoints
* [Assign node to a given path in a given hierarchy](hierarchy-api/assign-hierarchy.md)
* [Remove given node from a given hierarchy](hierarchy-api/remove-hierarchy.md)
* [Get a lists of all nodes in a given folder and the deeper ones](hierarchy-api/get-list.md)
* [Get a tree starting from a given directory](hierarchy-api/get-tree.md)
* [Get a tree starting from a given directory (version without parameters getting root of a hierarchy)](hierarchy-api/get-tree-no-params.md)
* [Get a list of all hierarchies](hierarchy-api/get-hierarchies.md)
* [Get a list of all hierarchies of a node](hierarchy-api/get-hierarchies-list.md)
* [Add multiple nodes to a given hierarchy](hierarchy-api/assign-hierarchy-multiple-nodes.md)
* [Remove multiple nodes to a given hierarchy](hierarchy-api/remove-hierarchy-multiple-nodes.md)

## Tests
* [hierarchy_test.go](src/app/backend/resource/hierarchy/hierarchy_test.go)
* [label_test.go](src/app/backend/resource/label/label_test.go)
* [common.go](src/github.com/kubernetes/dashboard/src/app/backend/testing/common.go)

### Geography extension
Dashboards frontend uses additional label assigned to nodes to display maps. The label name is `arm-geography` and it's format is `G<longitude>-<latitude>` eg. `G-83.034324--56.22333`. The letter G at the beginning allows to add points with first coordinate being negative, without breaking kubernetes naming concepts.

### Selection extension
The frontend also uses `arm-selection` label to add nodes to working set - the nodes are included in the set if the value of `arm-selection` is `true`.

### Changes to persistent dasboard configuration ConfigMap 
Some elements of the dashboard's frontend require persistence. Persistent data are stored in the `kubernetes-dashboard-settings` ConfigMap in `kube-system` namespace. The schema of this configMap in this fork differs from it's normal schema from standard dashboard. There are three new fields:
* `googleMapsKey` - string containing your google maps API key
* `deploymentLabelDetails` - string containing JSON with deployments details - eg. `[{"name":"app1", "labelsNames": ["label1", "label2"], "labelsContents": ["value1", "value2"]}]`
* `lastQuery`  - string storing the query which was ran before and which can be restored by clicking `load last query` in the dasboards GUI
